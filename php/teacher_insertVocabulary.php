﻿<?php
	
	session_start();
	header("Content-Type:text/html; charset=gb2312");
	include('connMysql.php');
	$account = $_SESSION['user'];
	
	$action = $_POST['action'];
	switch($action){
		case 'query':
			$page = isset($_POST['page']) ? $_POST['page'] : 1;
			$limit = isset($_POST['limit']) ? $_POST['limit'] : 3;
			
			$vlTheme = isset($_POST['vlTheme']) ? $_POST['vlTheme'] : '';
			$vlTitle = isset($_POST['vlTitle']) ? $_POST['vlTitle'] : '';
			$vlPractice = isset($_POST['vlPractice']) ? $_POST['vlPractice'] : '';
			$vlVocabulary = isset($_POST['vlDefinition']) ? $_POST['vlDefinition'] : '';
			
			$rowInit = ($limit * ($page - 1));
			try {
				//總筆數
				$sql = 
					"SELECT COUNT(*) AS count
					FROM vocabulary_library a
					INNER JOIN practice b on 
						a.vl_practice = b.pt_code 
						AND a.vl_title = b.kind_of_title 
						AND a.vl_theme = b.kind_of_theme
					INNER JOIN title c on 
						c.title_code = b.kind_of_title 
						AND c.kind_of_theme = b.kind_of_theme
					INNER JOIN theme d on d.theme_code = c.kind_of_theme";
				if($vlTheme != '' || $vlTitle != '' || $vlPractice != '' || $vlVocabulary != ''){
					$sql = $sql." WHERE 1 = 1";
					if($vlTheme != ''){
						$sql = $sql." AND a.vl_theme = :vlTheme";
					}
					if($vlTitle != ''){
						$sql = $sql." AND a.vl_title = :vlTitle";
					}
					if($vlPractice != ''){
						$sql = $sql." AND a.vl_practice = :vlPractice";
					}
					if($vlVocabulary != ''){
						$sql = $sql." AND a.vl_vocabulary LIKE :vlVocabulary";
					}
				}
				$stmt = $pdo->prepare($sql);
				if($vlTheme != ''){
					$stmt->bindValue(':vlTheme', $vlTheme);
				}
				if($vlTitle != ''){
					$stmt->bindValue(':vlTitle', $vlTitle);
				}
				if($vlPractice != ''){
					$stmt->bindValue(':vlPractice', $vlPractice);
				}
				if($vlVocabulary != ''){
					$stmt->bindValue(':vlVocabulary', '%'.$vlVocabulary.'%');
				}
				$stmt->execute() or exit("vocabulary_library，發生錯誤。"); //執行。
				// 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
				$resultCount = $stmt->fetchALL(PDO::FETCH_ASSOC);
				$result['total'] = $resultCount[0]['count'];
				//dataSource
				$sql = 
					"SELECT a.vl_id, a.vl_vocabulary, a.vl_definition, a.vl_part_of_speech, a.vl_theme, 
						a.vl_title, a.vl_practice, a.vl_upload_date, b.pt_name, c.title_name, 
						d.theme_name
					FROM vocabularyisland.vocabulary_library a
					INNER JOIN vocabularyisland.practice b on 
						a.vl_practice = b.pt_code 
						AND a.vl_title = b.kind_of_title 
						AND a.vl_theme = b.kind_of_theme
					INNER JOIN vocabularyisland.title c on 
						c.title_code = b.kind_of_title 
						AND c.kind_of_theme = b.kind_of_theme
					INNER JOIN vocabularyisland.theme d on d.theme_code = c.kind_of_theme";
				if($vlTheme != '' || $vlTitle != '' || $vlPractice != '' || $vlVocabulary != ''){
					$sql = $sql." WHERE 1 = 1";
					if($vlTheme != ''){
						$sql = $sql." AND a.vl_theme = :vlTheme";
					}
					if($vlTitle != ''){
						$sql = $sql." AND a.vl_title = :vlTitle";
					}
					if($vlPractice != ''){
						$sql = $sql." AND a.vl_practice = :vlPractice";
					}
					if($vlVocabulary != ''){
						$sql = $sql." AND a.vl_vocabulary LIKE :vlVocabulary";
					}
				}
				$sql = $sql." order by a.vl_id limit :rowInit, :limit";
				$stmt = $pdo->prepare($sql);
				if($vlTheme != ''){
					$stmt->bindValue(':vlTheme', $vlTheme);
				}
				if($vlTitle != ''){
					$stmt->bindValue(':vlTitle', $vlTitle);
				}
				if($vlPractice != ''){
					$stmt->bindValue(':vlPractice', $vlPractice);
				}
				if($vlVocabulary != ''){
					$stmt->bindValue(':vlVocabulary', '%'.$vlVocabulary.'%');
				}
				$stmt->bindValue(':rowInit', $rowInit, PDO::PARAM_INT);
				$stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
				$stmt->execute() or exit("vocabulary_library，發生錯誤。"); //執行。
				$result['records'] = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
				
			} catch (Exception $e) {
				$result = ['result' => false, 'message' => $e->getMessage()];
			}
			echo json_encode($result);
			break;
		case 'save':
			$vlTheme = $_POST['vlTheme'];
			$vlTitle = $_POST['vlTitle'];
			$vlPractice = $_POST['vlPractice'];
			$vlVocabulary = $_POST['vlVocabulary'];
			$vlDefinition = $_POST['vlDefinition'];
			$vlPartOfSpeech = $_POST['vlPartOfSpeech'];			
			
			$sql = 
				"SELECT COUNT(*) AS count
				FROM vocabularyisland.vocabulary_library 
				WHERE vl_theme = :vlTheme 
					AND vl_title = :vlTitle 
					AND vl_practice = :vlPractice ";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':vlTheme', $vlTheme);
			$stmt->bindValue(':vlTitle', $vlTitle);
			$stmt->bindValue(':vlPractice', $vlPractice);
			$stmt->execute() or exit("vocabulary_library，發生錯誤。"); //執行。
			$resultCount = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$count = $resultCount[0]['count'];
			
			/* 回傳狀態。*/
			$information = array();
			try{
				if($count == 4){
					$information['status'] = 'error';
					$information['msg'] = '此自主練習最多只能4個單字!!';
				} else {
					move_uploaded_file($_FILES["insertUpload_img1"]["tmp_name"], "../word_image/".$_FILES['insertUpload_img1']['name']);
					rename("../word_image/".$_FILES['insertUpload_img1']['name'], "../word_image/".$vlVocabulary."_0.jpg");

					move_uploaded_file($_FILES["insertUpload_img2"]["tmp_name"],"../word_image/".$_FILES['insertUpload_img2']['name']);
					rename("../word_image/".$_FILES['insertUpload_img2']['name'], "../word_image/".$vlVocabulary."_1.jpg");
					
					move_uploaded_file($_FILES["insertUpload_img3"]["tmp_name"],"../word_image/".$_FILES['insertUpload_img3']['name']);
					rename("../word_image/".$_FILES['insertUpload_img3']['name'], "../word_image/".$vlVocabulary."_2.jpg");
					
					$sql = 
						"INSERT INTO vocabularyisland.vocabulary_library (
							vl_vocabulary, vl_definition, vl_part_of_speech, 
							vl_theme, vl_title, vl_practice, vl_upload_date
						) VALUES (
							:vlVocabulary, :vlDefinition, :vlPartOfSpeech, 
							:vlTheme, :vlTitle, :vlPractice, now()
						)";
						
					$stmt = $pdo->prepare($sql);
					$stmt->bindValue(':vlVocabulary', $vlVocabulary);
					$stmt->bindValue(':vlDefinition', $vlDefinition);
					$stmt->bindValue(':vlPartOfSpeech', $vlPartOfSpeech);
					$stmt->bindValue(':vlTheme', $vlTheme);
					$stmt->bindValue(':vlTitle', $vlTitle);
					$stmt->bindValue(':vlPractice', $vlPractice);
					
					if($stmt->execute()){
						$information['status'] = 'success';
						$information['msg'] = '新增單字成功';
					}else{
						$information['status'] = 'error';
						$information['msg'] = '新增單字失敗';
					}
				}
				echo json_encode($information);
			}catch(Exception $e){
				$information['status'] = 'error';
				$information['msg'] = $e.getMessage();
				echo json_encode($information);
			}
			break;
		case 'update':
			$vlId = $_POST['vlId'];
			/*$vlTheme = $_POST['vlTheme'];
			$vlTitle = $_POST['vlTitle'];
			$vlPractice = $_POST['vlPractice'];*/
			$vlVocabulary = $_POST['vlVocabulary'];
			$vlDefinition = $_POST['vlDefinition'];
			$vlPartOfSpeech = $_POST['vlPartOfSpeech'];
			
			
			if(isset($_FILES["updateUpload_img1"]) != ''){
				unlink('../word_image/'.$vlVocabulary.'_0.jpg');
				move_uploaded_file($_FILES["updateUpload_img1"]["tmp_name"], "../word_image/".$_FILES['updateUpload_img1']['name']);
				rename("../word_image/".$_FILES['updateUpload_img1']['name'], "../word_image/".$vlVocabulary."_0.jpg");
			}
			if(isset($_FILES["updateUpload_img2"]) != ''){
				unlink('../word_image/'.$vlVocabulary.'_1.jpg');
				move_uploaded_file($_FILES["updateUpload_img2"]["tmp_name"],"../word_image/".$_FILES['updateUpload_img2']['name']);
				rename("../word_image/".$_FILES['updateUpload_img2']['name'], "../word_image/".$vlVocabulary."_1.jpg");
			}
			if(isset($_FILES["updateUpload_img3"]) != ''){
				unlink('../word_image/'.$vlVocabulary.'_2.jpg');
				move_uploaded_file($_FILES["updateUpload_img3"]["tmp_name"],"../word_image/".$_FILES['updateUpload_img3']['name']);
				rename("../word_image/".$_FILES['updateUpload_img3']['name'], "../word_image/".$vlVocabulary."_2.jpg");
			}
			$sql = 
				"UPDATE vocabularyisland.vocabulary_library SET
					vl_vocabulary = :vlVocabulary, vl_definition = :vlDefinition, vl_part_of_speech = :vlPartOfSpeech
				WHERE vl_id = :vlId";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':vlVocabulary', $vlVocabulary);
			$stmt->bindValue(':vlDefinition', $vlDefinition);
			$stmt->bindValue(':vlPartOfSpeech', $vlPartOfSpeech);
			$stmt->bindValue(':vlId', $vlId);
			
			if($stmt->execute()){
				$information['status'] = 'success';
				$information['msg'] = '修改單字完成';
			}else{
				$information['status'] = 'error';
				$information['msg'] = '修改單字失敗';
			}
			echo json_encode($information);
			break;
		case "delete":
			$vlId = $_POST['vlId'];
			$vlVocabulary = $_POST['vlVocabulary'];
			
			$information = array();
			if(file_exists("../word_image/".$vlVocabulary."_0.jpg")){
				unlink('../word_image/'.$vlVocabulary.'_0.jpg');
			}
			if(file_exists("../word_image/".$vlVocabulary."_1.jpg")){
				unlink('../word_image/'.$vlVocabulary.'_1.jpg');
			}
			if(file_exists("../word_image/".$vlVocabulary."_2.jpg")){
				unlink('../word_image/'.$vlVocabulary.'_2.jpg');
			}
			$sql = 
				"DELETE FROM vocabularyisland.vocabulary_library
				WHERE vl_id = :vlId";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':vlId', $vlId);
			/* 回傳狀態。*/
			$information = array();
			if($stmt->execute()){
				$information['status'] = 'success';
				$information['msg'] = '刪除單字完成';
			}else{
				$information['status'] = 'error';
				$information['msg'] = '刪除單字失敗';
			}
			echo json_encode($information);
			
			break;
		case "getThemeNameMenu":
			$sql = 
				"SELECT theme_code, theme_name FROM vocabularyisland.theme ORDER BY theme_code";
			$stmt = $pdo->prepare($sql);
			$stmt->execute() or exit("theme，發生錯誤。"); //執行。
			// 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$result['theme'] = $stmt->fetchALL(PDO::FETCH_ASSOC); 
			
			echo json_encode($result);
			break;
		case "getTitleNameMenu":
			$vlTheme = $_POST['vlTheme'];
			
			$sql = 
				"SELECT title_code, title_name
				FROM vocabularyisland.title
				WHERE kind_of_theme = :vlTheme
				ORDER BY title_code";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':vlTheme', $vlTheme);
			$stmt->execute() or exit("title，發生錯誤。"); //執行。
			 // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$result['title'] = $stmt->fetchALL(PDO::FETCH_ASSOC);
			
			echo json_encode($result);
			break;
		case "getPracticeNameMenu":
			$vlTheme = $_POST['vlTheme'];
			$vlTitle = $_POST['vlTitle'];
			$sql = 
				"SELECT pt_code, pt_name 
				FROM vocabularyisland.practice 
				WHERE kind_of_theme = :kindOfTheme 
					AND kind_of_title = :kindOfTitle 
				ORDER BY pt_code";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':kindOfTheme', $vlTheme);
			$stmt->bindValue(':kindOfTitle', $vlTitle);
			$stmt->execute() or exit("practice，發生錯誤。"); //執行。
			 // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$result['title'] = $stmt->fetchALL(PDO::FETCH_ASSOC);
			
			echo json_encode($result);
			break;
	}    
?>