<?php

	session_start();

	include('connMysql.php');
	$account = $_SESSION['user'];
	
	$action = $_POST['action'];
	switch($action){
		case 'query':
			$page = isset($_POST['page']) ? $_POST['page'] : 1;
			$limit = isset($_POST['limit']) ? $_POST['limit'] : 3;
			
			$kindOfTheme = isset($_POST['kindOfTheme']) ? $_POST['kindOfTheme']: '';
			$titleName = isset($_POST['titleName']) ? $_POST['titleName']: '';
			
			$rowInit = ($limit * ($page - 1));
			try {
				//總筆數
				$sql = "SELECT COUNT(*) AS count FROM vocabularyisland.title ";
				if($kindOfTheme != '' || $titleName != ''){
					$sql = $sql."WHERE 1 = 1 ";
					if($kindOfTheme != ''){
						$sql = $sql."AND kind_of_theme = :kindOfTheme ";
					}
					if($titleName != ''){
						$sql = $sql."AND title_name like :titleName ";
					}
				}
				
				$stmt = $pdo->prepare($sql);
				if($kindOfTheme != ''){
					$stmt->bindValue(':kindOfTheme', $kindOfTheme);
				}
				if($titleName != ''){
					$stmt->bindValue(':titleName', '%'.$titleName.'%');
				}
				$stmt->execute() or exit("theme，發生錯誤。"); //執行。
				$resultCount = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
				$result['total'] = $resultCount[0]['count'];
				
				$sql = 
					"SELECT a.title_id, a.title_name, a.title_code +1 as title_code, a.kind_of_theme, b.theme_name
					FROM vocabularyisland.title a
					INNER JOIN vocabularyisland.theme b ON a.kind_of_theme = b.theme_code ";
				if($kindOfTheme != '' || $titleName != ''){
					$sql = $sql."WHERE 1 = 1 ";
					if($kindOfTheme != ''){
						$sql = $sql."AND kind_of_theme = :kindOfTheme ";
					}
					if($titleName != ''){
						$sql = $sql."AND title_name like :titleName ";
					}
				}
				$sql = $sql."order by a.title_code limit :rowInit, :limit";
				$stmt = $pdo->prepare($sql);
				if($kindOfTheme != ''){
					$stmt->bindValue(':kindOfTheme', $kindOfTheme);
				}
				if($titleName != ''){
					$stmt->bindValue(':titleName', '%'.$titleName.'%');
				}
				$stmt->bindValue(':rowInit', $rowInit, PDO::PARAM_INT);
				$stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
				$stmt->execute() or exit("theme，發生錯誤。"); //執行。
				$result['records'] = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
				
			} catch (Exception $e) {
				$result = ['result' => false, 'message' => $e->getMessage()];
			}
			echo json_encode($result);
			break;
		case 'save':
			$titleName = $_POST['titleName'];
			$kindOfTheme = $_POST['kindOfTheme'];
			$titleCode = $_POST['titleCode'] -1;
			
			$sql = 
				"SELECT COUNT(*) AS count FROM vocabularyisland.title 
				WHERE kind_of_theme = :kindOfTheme
					AND title_code = :titleCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':kindOfTheme', $kindOfTheme, PDO::PARAM_INT);
			$stmt->bindValue(':titleCode', $titleCode, PDO::PARAM_INT);
			$stmt->execute() or exit("theme，發生錯誤。"); //執行。
			$resultCount = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$count = $resultCount[0]['count'];
			
			if($count > 0){
				$information['status'] = 'error';
				$information['msg'] = '此子主題順序已存在';
				echo json_encode($information);
				break;
			}else{
				$sql = 
				"INSERT INTO 
				vocabularyisland.title (
					title_name, title_code, kind_of_theme
				) values (
					:titleName, :titleCode, :kindOfTheme
				)";
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(':titleName',$titleName);
				$stmt->bindValue(':titleCode',$titleCode, PDO::PARAM_INT);
				$stmt->bindValue(':kindOfTheme',$kindOfTheme, PDO::PARAM_INT);
				/* 回傳狀態。*/
				$information = array();
				if ($stmt->execute()) { 
					$information['status'] = 'success';
					$information['msg'] = '儲存子主題成功';
				} else {
					$information['status'] = 'error';
					$information['results'] = $stmt->error;
					$information['msg'] = '儲存子主題失敗';
				}
				echo json_encode($information);
				break;
				
			}
			
		case 'update':
			$titleId = $_POST['titleId'];
			$titleName = $_POST['titleName'];
			$titleCode = $_POST['titleCode'] -1;
			
			$sql = "SELECT COUNT(*) AS count FROM vocabularyisland.title WHERE title_code = :titleCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':titleCode', $titleCode, PDO::PARAM_INT);
			$stmt->execute() or exit("theme，發生錯誤。"); //執行。
			$resultCount = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$count = $resultCount[0]['count'];
			
			if($count > 0){
				$information = array();
				$information['status'] = 'error';
				$information['msg'] = '此子主題順序已存在';
				echo json_encode($information);
				break;
			}else{
				$sql = 
					"UPDATE vocabularyisland.title
					SET title_name = :titleName, title_code = :titleCode
					WHERE title_id = :titleId";
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(':titleName',$titleName);
				$stmt->bindValue(':titleCode',$titleCode);
				$stmt->bindValue(':titleId',$titleId);
				/* 回傳狀態。*/
				$information = array();
				if ($stmt->execute()) { 
					$information['result'] = 'success';
					$information['msg'] = '修改子主題成功';
				} else {
					$information['result'] = $stmt->error;
					$information['result'] = 'error';
					$information['msg'] = '修改子主題失敗';
				}
					
				echo json_encode($information);
				break;
			}
		case "delete":
			$titleId = $_POST['titleId'];
			$kindOfTheme = $_POST['kindOfTheme'];
			$titleCode = $_POST['titleCode'] -1;
			//刪除單字圖檔
			$sql = 
				"SELECT vl_vocabulary
				FROM vocabularyisland.vocabulary_library
				WHERE vl_theme = :kindOfTheme
					AND vl_title = :titleCode";
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(':kindOfTheme',$kindOfTheme);
				$stmt->bindValue(':titleCode',$titleCode);
			$stmt->execute() or exit("vocabulary_library，發生錯誤。"); //執行。
			// 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$result['vlVocabulary'] = $stmt->fetchALL(PDO::FETCH_ASSOC);
			for( $i = 0 ; $i< count($result['vlVocabulary']) ; $i++){
				$vlVocabulary = $result['vlVocabulary'][$i]['vl_vocabulary'];
				if(file_exists("../word_image/".$vlVocabulary."_0.jpg")){
					unlink('../word_image/'.$vlVocabulary.'_0.jpg');
				}
				if(file_exists("../word_image/".$vlVocabulary."_1.jpg")){
					unlink('../word_image/'.$vlVocabulary.'_1.jpg');
				}
				if(file_exists("../word_image/".$vlVocabulary."_2.jpg")){
					unlink('../word_image/'.$vlVocabulary.'_2.jpg');
				}
			}
			//刪除單字
			$sql = 
				"DELETE FROM vocabularyisland.vocabulary_library
				WHERE vl_theme = :kindOfTheme
					AND vl_title = :titleCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':kindOfTheme',$kindOfTheme);
			$stmt->bindValue(':titleCode',$titleCode);
			$stmt->execute() or exit("vocabulary_library，發生錯誤。"); //執行。
			//刪除自主練習
			$sql = 
				"DELETE FROM vocabularyisland.practice
				WHERE kind_of_theme = :kindOfTheme
					AND kind_of_title = :titleCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':kindOfTheme',$kindOfTheme);
			$stmt->bindValue(':titleCode',$titleCode);
			$stmt->execute() or exit("practice，發生錯誤。"); //執行。
			//刪除子主題
			$sql = 
				"DELETE FROM vocabularyisland.title
				WHERE title_id = :titleId";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':titleId',$titleId);
			$stmt->execute() or exit("title，發生錯誤。"); //執行。
			
			$information = array();
			$information["status"] = "success";
			$information["msg"] = "刪除子主題成功";
			echo json_encode($information);
			break;
		case "getThemeName":
			$sql = 
				"SELECT theme_code, theme_name
				FROM vocabularyisland.theme";
			$stmt = $pdo->prepare($sql);
			$stmt->execute() or exit("theme，發生錯誤。"); //執行。
			$result['records'] = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			echo json_encode($result);
			break;
	}    
?>