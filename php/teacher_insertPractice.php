<?php

	session_start();

	include('connMysql.php');
	$account = $_SESSION['user'];
	
	$action = $_POST['action'];
	switch($action){
		case 'query':
			$page = isset($_POST['page']) ? $_POST['page'] : 1;
			$limit = isset($_POST['limit']) ? $_POST['limit'] : 3;
			
			$kindOfTheme = isset($_POST['kindOfTheme']) ? $_POST['kindOfTheme'] : '';
			$kindOfTitle = isset($_POST['kindOfTitle']) ? $_POST['kindOfTitle'] : '';
			$ptName = isset($_POST['ptName']) ? $_POST['ptName'] : '';
			
			$rowInit = ($limit * ($page - 1));
			try {
				//總筆數
				$sql = 
					"SELECT COUNT(*) AS count 
					FROM vocabularyisland.practice a
					INNER JOIN title b on a.kind_of_title = b.title_code and a.kind_of_theme = b.kind_of_theme
					INNER JOIN theme c on b.kind_of_theme = c.theme_code";
				if($kindOfTheme != '' || $kindOfTitle != '' || $ptName != ''){
					$sql = $sql." WHERE 1 = 1";
					if($kindOfTheme != ''){
						$sql = $sql." AND a.kind_of_theme = :kindOfTheme";
					}
					if($kindOfTitle != ''){
						$sql = $sql." AND a.kind_of_title = :kindOfTitle";
					}
					if($ptName != ''){
						$sql = $sql." AND a.pt_name LIKE :ptName";
					}
				}
				$stmt = $pdo->prepare($sql);
				if($kindOfTheme != ''){
					$stmt->bindValue(':kindOfTheme', $kindOfTheme);
				}
				if($kindOfTitle != ''){
					$stmt->bindValue(':kindOfTitle', $kindOfTitle);
				}
				if($ptName != ''){
					$stmt->bindValue(':ptName', '%'.$ptName.'%');
				}
				$stmt->execute() or exit("practice，發生錯誤。"); //執行。
				$resultCount = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
				$result['total'] = $resultCount[0]['count'];
				
				$sql = 
					"SELECT a.pt_id, a.pt_name, a.pt_code, a.kind_of_title, a.kind_of_theme,
						b.title_name, c.theme_name
					FROM vocabularyisland.practice a
					INNER JOIN title b on a.kind_of_title = b.title_code and a.kind_of_theme = b.kind_of_theme
					INNER JOIN theme c on b.kind_of_theme = c.theme_code";
				if($kindOfTheme != '' || $kindOfTitle != '' || $ptName != ''){
					$sql = $sql." WHERE 1 = 1";
					if($kindOfTheme != ''){
						$sql = $sql." AND a.kind_of_theme = :kindOfTheme";
					}
					if($kindOfTitle != ''){
						$sql = $sql." AND a.kind_of_title = :kindOfTitle";
					}
					if($ptName != ''){
						$sql = $sql." AND a.pt_name LIKE :ptName";
					}
				}
				$sql = $sql." order by a.pt_id limit :rowInit, :limit";
				$stmt = $pdo->prepare($sql);
				if($kindOfTheme != ''){
					$stmt->bindValue(':kindOfTheme', $kindOfTheme);
				}
				if($kindOfTitle != ''){
					$stmt->bindValue(':kindOfTitle', $kindOfTitle);
				}
				if($ptName != ''){
					$stmt->bindValue(':ptName', '%'.$ptName.'%');
				}
				$stmt->bindValue(':rowInit', $rowInit, PDO::PARAM_INT);
				$stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
				$stmt->execute() or exit("practice，發生錯誤。"); //執行。
				$result['records'] = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
				
			} catch (Exception $e) {
				$result = ['result' => false, 'message' => $e->getMessage()];
			}
			echo json_encode($result);
			break;
		case 'save':
			$kindOfTheme = $_POST['kindOfTheme'];
			$kindOfTitle = $_POST['kindOfTitle'];
			$ptName = $_POST['ptName'];
			
			$sql = 
				"SELECT pt_code 
				FROM vocabularyisland.practice 
				WHERE kind_of_theme = :kindOfTheme 
					AND kind_of_title = :kindOfTitle 
				ORDER BY pt_code DESC LIMIT 1";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':kindOfTheme', $kindOfTheme);
			$stmt->bindValue(':kindOfTitle', $kindOfTitle);
			$stmt->execute() or exit("theme，發生錯誤。"); //執行。
			$resultCount = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			
			$ptCode = $resultCount == null? 0 : $resultCount[0]['pt_code']+1 ;
			$sql = 
				"INSERT INTO 
				vocabularyisland.practice (
					pt_name, pt_code, kind_of_title, kind_of_theme
				) values (
					:ptName, :ptCode, :kindOfTitle, :kindOfTheme
				)";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':ptName',$ptName);
			$stmt->bindValue(':ptCode',$ptCode, PDO::PARAM_INT);
			$stmt->bindValue(':kindOfTitle',$kindOfTitle);
			$stmt->bindValue(':kindOfTheme',$kindOfTheme);
			/* 回傳狀態。*/
			$information = array();
			if ($stmt->execute()) { 
				$information['status'] = 'success';
				$information['msg'] = '新增自主練習完成';
			} else {
				$information['status'] = 'error';
				$information['msg'] = '新增自主練習失敗';
			}
			echo json_encode($information);
			break;
		case 'update':
			$ptId = $_POST['ptId'];
			$ptName = $_POST['ptName'];

			$sql = 
				"UPDATE vocabularyisland.practice
				SET pt_name = :ptName
				WHERE pt_id = :ptId";
				$stmt = $pdo->prepare($sql);
				$stmt->bindValue(':ptName',$ptName);
				$stmt->bindValue(':ptId',$ptId);
			/* 回傳狀態。*/
			$information = array();
			if($stmt->execute()){
				$information['status'] = 'success';
				$information['msg'] = '修改自主練習完成';
			}else{
				$information['status'] = 'error';
				$information['msg'] = '修改自主練習失敗';
			}
				
			echo json_encode($information);
			break;
		case 'delete':
			$ptId = $_POST['ptId'];
			$ptCode = $_POST['ptCode'];
			$kindOfTitle = $_POST['kindOfTitle'];
			$kindOfTheme = $_POST['kindOfTheme'];
			
			$sql = 
				"SELECT vl_vocabulary
				FROM vocabularyisland.vocabulary_library
				WHERE vl_theme = :kindOfTheme
					AND vl_title = :kindOfTitle
					AND vl_practice = :ptCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':kindOfTheme',$kindOfTheme);
			$stmt->bindValue(':kindOfTitle',$kindOfTitle);
			$stmt->bindValue(':ptCode',$ptCode);
			$stmt->execute() or exit("vocabulary_library，發生錯誤。"); //執行。
			// 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$result['vlVocabulary'] = $stmt->fetchALL(PDO::FETCH_ASSOC);
			//刪除單字圖檔
			for( $i = 0 ; $i< count($result['vlVocabulary']) ; $i++){
				$vlVocabulary = $result['vlVocabulary'][$i]['vl_vocabulary'];
				if(file_exists("../word_image/".$vlVocabulary."_0.jpg")){
					unlink('../word_image/'.$vlVocabulary.'_0.jpg');
				}
				if(file_exists("../word_image/".$vlVocabulary."_1.jpg")){
					unlink('../word_image/'.$vlVocabulary.'_1.jpg');
				}
				if(file_exists("../word_image/".$vlVocabulary."_2.jpg")){
					unlink('../word_image/'.$vlVocabulary.'_2.jpg');
				}
			}
			//刪除單字
			$sql = 
				"DELETE FROM vocabularyisland.vocabulary_library
				WHERE vl_theme = :kindOfTheme
					AND vl_title = :kindOfTitle
					AND vl_practice = :ptCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':kindOfTheme',$kindOfTheme);
			$stmt->bindValue(':kindOfTitle',$kindOfTitle);
			$stmt->bindValue(':ptCode',$ptCode);
			$stmt->execute() or exit("vocabulary_library，發生錯誤。"); //執行。
			//刪除自主練習
			$sql = 
				"DELETE FROM vocabularyisland.practice
				WHERE pt_id = :ptId";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':ptId',$ptId);
			$stmt->execute() or exit("practice，發生錯誤。"); //執行。
			
			$information = array();
			$information["status"] = "success";
			$information["msg"] = "刪除自我練習成功";
			echo json_encode($information);
			break;
		case "getThemeNameMenu":
			$sql = 
				"SELECT theme_code, theme_name 
				FROM vocabularyisland.theme 
				ORDER BY theme_code";
			$stmt = $pdo->prepare($sql);
			$stmt->execute() or exit("theme，發生錯誤。"); //執行。
			$result['theme'] = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			
			echo json_encode($result);
			break;
		case "getTitleNameMenu":
			$kindOfTheme = $_POST["kindOfTheme"];
			$sql = 
				"SELECT title_code, title_name 
				FROM vocabularyisland.title";
			if($kindOfTheme != ''){
				$sql = $sql." WHERE kind_of_theme = :kindOfTheme";
			}
			$sql = $sql." ORDER BY title_code";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':kindOfTheme', $kindOfTheme);
			$stmt->execute() or exit("title，發生錯誤。"); //執行。
			$result['title'] = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			
			echo json_encode($result);
			break;
	}    
?>