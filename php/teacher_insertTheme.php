<?php

	session_start();

	include('connMysql.php');
	$account = $_SESSION['user'];
	
	$action = $_POST['action'];
	switch($action){
		case 'query':
			$page = isset($_POST['page']) ? $_POST['page'] : 1;
			$limit = isset($_POST['limit']) ? $_POST['limit'] : 3;
			$theme = isset($_POST['theme']) ? $_POST['theme']: '';
			
			$rowInit = ($limit * ($page - 1));
			try {
				//總筆數
				$sql = "SELECT COUNT(*) AS count FROM vocabularyisland.theme";
				$stmt = $pdo->prepare($sql);
				$stmt->execute() or exit("theme，發生錯誤。"); //執行。
				$resultCount = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
				$result['total'] = $resultCount[0]['count'];
				
				$sql = "SELECT theme_id, theme_name, theme_code FROM vocabularyisland.theme ";
				if($theme != ''){
					$sql = $sql."WHERE theme = :theme ";
				}
				$sql = $sql."order by theme_code limit :rowInit, :limit";
				$stmt = $pdo->prepare($sql);
				if($theme != ''){
					$stmt->bindValue(':theme', $theme);
				}
				$stmt->bindValue(':rowInit', $rowInit, PDO::PARAM_INT);
				$stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
				$stmt->execute() or exit("theme，發生錯誤。"); //執行。
				$result['records'] = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
				
			} catch (Exception $e) {
				$result = ['result' => false, 'message' => $e->getMessage()];
			}
			echo json_encode($result);
			break;
		case 'save':
			$themeName = $_POST['themeName'];
			
			$sql = "SELECT theme_code FROM `theme` ORDER BY theme_code DESC LIMIT 1";
			$stmt = $pdo->prepare($sql);
			$stmt->execute() or exit("theme，發生錯誤。"); //執行。
			$resultCount = $stmt->fetchALL(PDO::FETCH_ASSOC); // 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$themeCode = $resultCount[0]['theme_code'];
			
			$sql = 
				"INSERT INTO 
				vocabularyisland.theme (
					theme_name, theme_code
				) values (
					:themeName, :themeCode
				)";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':themeName',$themeName);
			$stmt->bindValue(':themeCode',$themeCode+1, PDO::PARAM_INT);
			/* 回傳狀態。*/
			$information = array();
			if ($stmt->execute()) { 
				$information['status'] = 'success';
				$information['msg'] = '新增主題成功!!';
			} else {
				$information['results'] = $stmt->error;
				$information['status'] = 'error';
				$information['msg'] = '新增主題失敗!!';
			}
			echo json_encode($information);
			break;
		case 'update':
			$themeId = $_POST['themeId'];
			$themeName = $_POST['themeName'];
			$information = array();
			$sql = 
				"UPDATE vocabularyisland.theme 
				SET theme_name = :themeName
				WHERE theme_id = :themeId";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':themeName',$themeName);
			$stmt->bindValue(':themeId',$themeId);
			/* 回傳狀態。*/
			if ($stmt->execute()) { 
				$information['status'] = 'success';
				$information['msg'] = '修改主題成功!!';
			} else {
				$information['result'] = $stmt->error;
				$information['status'] = 'error';
				$information['msg'] = '修改主題失敗!!';
			}
			
			echo json_encode($information);
			break;
		case 'delete':
			$themeId = $_POST['themeId'];
			$themeCode = $_POST['themeCode'];
			
			//刪除單字圖檔
			$sql = 
				"SELECT vl_vocabulary
				FROM vocabularyisland.vocabulary_library
				WHERE vl_theme = :themeCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':themeCode',$themeCode);
			$stmt->execute() or exit("vocabulary_library，發生錯誤。"); //執行。
			// 將帳號資料照索引順序一一全部取出，並以陣列放入$row。
			$result['vlVocabulary'] = $stmt->fetchALL(PDO::FETCH_ASSOC);
			
			for( $i = 0 ; $i< count($result['vlVocabulary']) ; $i++){
				$vlVocabulary = $result['vlVocabulary'][$i]['vl_vocabulary'];
				if(file_exists("../word_image/".$vlVocabulary."_0.jpg")){
					unlink('../word_image/'.$vlVocabulary.'_0.jpg');
				}
				if(file_exists("../word_image/".$vlVocabulary."_1.jpg")){
					unlink('../word_image/'.$vlVocabulary.'_1.jpg');
				}
				if(file_exists("../word_image/".$vlVocabulary."_2.jpg")){
					unlink('../word_image/'.$vlVocabulary.'_2.jpg');
				}
			}
			//刪除單字
			$sql = 
				"DELETE FROM vocabularyisland.vocabulary_library
				WHERE vl_theme = :themeCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':themeCode',$themeCode);
			$stmt->execute() or exit("vocabulary_library，發生錯誤。"); //執行。
			
			//刪除自主練習
			$sql = 
				"DELETE FROM vocabularyisland.practice
				WHERE kind_of_theme = :themeCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':themeCode',$themeCode);
			$stmt->execute() or exit("practice，發生錯誤。"); //執行。
			//刪除子主題
			$sql = 
				"DELETE FROM vocabularyisland.title
				WHERE kind_of_theme = :themeCode";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':themeCode',$themeCode);
			$stmt->execute() or exit("title，發生錯誤。"); //執行。
			
			//刪除主題
			$sql = 
				"DELETE FROM vocabularyisland.theme
				WHERE theme_id = :themeId";
			$stmt = $pdo->prepare($sql);
			$stmt->bindValue(':themeId',$themeId);
			$stmt->execute() or exit("title，發生錯誤。"); //執行。
			
			$information = array();
			/* 回傳狀態。*/
			if ($stmt->execute()) { 
				$information['status'] = 'success';
				$information['msg'] = '刪除主題成功!!';
			} else {
				$information['result'] = $stmt->error;
				$information['status'] = 'error';
				$information['msg'] = '刪除主題失敗!!';
			}
			
			echo json_encode($information);
			break;
	}    
?>