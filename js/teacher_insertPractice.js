var grid, onSuccessFunc = function( response ) {
	/*自訂項次*/
	var page = 1;
	var responseJson = JSON.parse(response);
	if( $("#divtab select[data-role='page-size']").val() < responseJson.total ) {
		page = $("#divtab input[data-role='page-number']").val() == 0 ? "1" : $("#divtab input[data-role='page-number']").val();
		console.dir( "page-number : " + page );
	}
	$.each(responseJson.records, function( index, value ) {
		var strPage = page -1 == 0 ? "" : (page-1).toString();
		responseJson.records[index]['rowId'] = index+1 == 10 ? parseInt( responseJson.records[index-1]['rowId'])+1 : strPage+(index+1);
	});
	//detail_grid render item number
	grid.render( responseJson );
	
	if( responseJson.rtnFlag == "ng" ){
		alert( responseJson.rtnMsg );
	}
};
$(document).ready(function () {
    let account = "";           // 輸入的帳號。
    let name = "";              // 使用者姓名。
    let number_of_card = 0;     // 字卡數量。
    var islands_status = [];    // 每個島嶼進度。
    
    
    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: true,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes !'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
            
        }else if(status == 1){
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";
        
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: false,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'O K'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
		}
    }
	
	/*抓使用者資訊。*/
    function getPersonalInformation() {
        console.log("開始抓使用者資訊...");
        
        $.ajax({
            type: "get",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                $('#name').text(name);
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                number_of_card = 0;
                $('#name').text(name);
            }
        });
        /* 未完成 */
    }
	
	/**
	 * 將表單資料序列化並轉成JSON型態
	 */
	$.fn.serializeFormJSON = function () {

		var o = {};
		var a = this.serializeArray();
		$.each(a, function () {
			if( o[this.name] ) {
				if( !o[this.name].push ) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

    /*上一頁*/
    $('#boat').on('click',function(){
        console.log('將回到主畫面。');
        location.replace("teacher.html");
    });
	
	grid = $('#grid').grid({
		dataSource: {
			type:"POST",
			url:'php/teacher_insertPractice.php',
			data:{
				action:'query'
			},
			success: onSuccessFunc
		},
		uiLibrary : 'bootstrap4',
		locale : 'zh-tw',
		columns: [{ 
			field: 'rowId', 
			title: '',
			width:50
		}, {
			field: 'pt_name', title: '自我練習'
		}, {
			field: 'pt_code', hidden: true
		}, {
			field: 'title_name', title: '子主題'
		}, { 
			field: 'kind_of_title', hidden: true
		}, {
			field: 'theme_name', title: '主題'
		}, { 
			field: 'kind_of_theme', hidden: true
		}, { 
			field: 'pt_id', hidden: true
		}, {
			field : "",
			title : "",
			tmpl : "<button type='button' id='updateBtn' class='btn btn-primary'><span class='btn-txt'>編輯</span></button>",
			events : {
				'click': update_view
			},
			width:100
		}/*, {
			field : "",
			title : "",
			tmpl : "<button type='button' id='deleteBtn' class='btn btn-primary'><span class='btn-txt'>刪除</span></button>",
			events : {
				'click': delete_view
			},
			width:100
		}*/],
		pager: {
			limit: 3,
			sizes: [1, 3, 5]
		}
	});

	//新增dialog
	$( "#insertPractice_dialog" ).dialog({
		autoOpen: false,
		height: 330,
		width: 370,
		modal: false,
		buttons:{
			"新增":function(){
				insertPractice();
			},
			"取消":function(){
				$("#insertPractice_dialog" ).dialog("close");
			}
		}
    });
	//編輯dialog
	$( "#updatePractice_dialog" ).dialog({
		autoOpen: false,
		height: 330,
		width: 370,
		modal: false,
		buttons:{
			"更新":function(){
				updatePractice();
			},
			"取消":function(){
				$("#updatePractice_dialog" ).dialog("close");
			}
		}
    });
	//新增button
	$("#insertPracticeBtn").on('click', function(){
		//open dialog
		$("#insertPracticeForm")[0].reset();
		getThemeNameMenu("save");
		$("#insertPractice_dialog" ).dialog("open");
	});
	
	//查詢button
	$("#selectPracticeBtn").on("click", function() {
		queryByPage();
	});
	
	//查詢條件-主題
	$("#dataForm #kindOfTheme").on("change", function() {
		if($("#dataForm #kindOfTheme").val() != ''){
			getTitleNameMenu("init", $("#dataForm #kindOfTheme").val());
		}else{
			$("#dataForm #kindOfTitle").empty();
			$("#dataForm #kindOfTitle").append($("<option></option>").attr("value", "").text("請選擇"));
		}
	});
	
	//新增dialog-主題
	$("#insertPracticeForm #kindOfTheme").on("change", function() {
		if($("#insertPracticeForm #kindOfTheme").val() != ''){
			getTitleNameMenu("save", $("#insertPracticeForm #kindOfTheme").val());
		}else{
			$("#insertPracticeForm #kindOfTitle").empty();
			$("#insertPracticeForm #kindOfTitle").append($("<option></option>").attr("value", "").text("請選擇"));
		}
	});
	
    /*---------------------------------------------------------------------*/
	
    getPersonalInformation();
	getThemeNameMenu("init");
});

	//查詢
	function queryByPage() {
		
		grid.reload( $("#dataForm").serializeFormJSON() );
	}
	//新增
	function insertPractice(){
		var rtnMsg = '';
		if($("#insertPracticeForm #kindOfTheme").val() == ''){
			rtnMsg+= "請選擇主題。";
		}
		if($("#insertPracticeForm #kindOfTitle").val() == ''){
			rtnMsg+= "請輸入子主題。";
		}
		if($("#insertPracticeForm #ptName").val() == ''){
			rtnMsg+= "請自主練習。";
		}
		if(rtnMsg != ''){
			swal.fire({
				icon: "error",
				title: "請輸入欄位",
				text:rtnMsg,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'OK !'
			});
			return;
		}
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertPractice.php",
			data:$("#insertPracticeForm").serializeFormJSON(),
			dataType: "json",
			success: function (json) {
				//console.log(json);
				if(json.status == 'success'){
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					}).then((result) => {
						if (result.value) {
							$("#insertPractice_dialog" ).dialog("close");
							grid.reload( $("#dataForm").serializeFormJSON() );
						}
					});
					
				}else{
					swal.fire({
						icon: "error",
						title: "新增自主練習失敗",
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'O K'
					});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
				swal.fire({
					icon: "error",
					title: "新增自主練習失敗",
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'O K'
				});
			}
		});
	}
	//顯示編輯dialog
	function update_view(e){
		getThemeNameMenu("update", e.data.record.kind_of_theme, e.data.record.kind_of_title);
		getTitleNameMenu("update", e.data.record.kind_of_theme, e.data.record.kind_of_title);
		$("#updatePracticeForm #practicId").val(e.data.record.pt_id);
		$("#updatePracticeForm #ptName").val(e.data.record.pt_name);
		
		$("#updatePractice_dialog" ).dialog("open");
	}
	//編輯
	function updatePractice(){
		var rtnMsg = '';
		if($("#updatePracticeForm #kindOfTheme").val() == ''){
			rtnMsg+= "請選擇主題。";
		}
		if($("#updatePracticeForm #kindOfTitle").val() == ''){
			rtnMsg+= "請輸入子主題。";
		}
		if($("#updatePracticeForm #ptName").val() == ''){
			rtnMsg+= "請自主練習。";
		}
		if(rtnMsg != ''){
			swal.fire({
				icon: "error",
				title: "請輸入欄位",
				text:rtnMsg,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'OK !'
			});
			return;
		}
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertPractice.php",
			data:$("#updatePracticeForm").serializeFormJSON(),
			dataType: "json",
			success: function (json) {
				if(json.status = 'success'){
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					}).then((result) => {
						if (result.value) {
							$("#updatePractice_dialog" ).dialog("close");
							grid.reload( $("#dataForm").serializeFormJSON() );
						}
					});
					
				}else{
					swal.fire({
						icon: "error",
						title: "修改自主練習失敗",
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'O K'
					});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
				swal.fire({
					icon: "error",
					title: "修改自主練習失敗",
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'O K'
				});
			}
		});
	}
	
	function delete_view(e){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertPractice.php",
			data:{
				action : "delete",
				ptId : e.data.record.pt_id,
				ptCode : e.data.record.pt_code,
				kindOfTitle : e.data.record.kind_of_title,
				kindOfTheme : e.data.record.kind_of_theme
			},
			dataType: "json",
			success: function (json) {
				console.log(json);
				if(json.status = 'success'){
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					}).then((result) => {
						if (result.value) {
							grid.reload( $("#dataForm").serializeFormJSON() );
						}
					});
					
				}else{
					swal.fire({
						icon: "error",
						title: "刪除自主練習失敗",
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'O K'
					});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
				swal.fire({
					icon: "error",
					title: "刪除自主練習失敗",
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'O K'
				});
			}
		});
	}
	
	//init取主題所有參數
	function getThemeNameMenu(formData, kindOfTheme){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertPractice.php",
			data:{
				action:"getThemeNameMenu"
			},
			dataType: "json",
			success: function (json) {
				if("init" == formData){
					$("#dataForm #kindOfTheme").empty();
					$("#dataForm #kindOfTheme").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.theme.length; i++){
						$("#dataForm #kindOfTheme").append($("<option></option>").attr("value", json.theme[i].theme_code).text(json.theme[i].theme_name));
					}
				}
				if("save" == formData){
					$("#insertPracticeForm #kindOfTheme").empty();
					$("#insertPracticeForm #kindOfTheme").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.theme.length; i++){
						$("#insertPracticeForm #kindOfTheme").append($("<option></option>").attr("value", json.theme[i].theme_code).text(json.theme[i].theme_name));
					}
				}
				if("update" == formData){
					$("#updatePracticeForm #kindOfTheme").empty();
					$("#updatePracticeForm #kindOfTheme").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.theme.length; i++){
						$("#updatePracticeForm #kindOfTheme").append($("<option></option>").attr("value", json.theme[i].theme_code).text(json.theme[i].theme_name));
					}
					$("#updatePracticeForm #kindOfTheme").val(kindOfTheme);
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
			}
		});
	}
	
	//init取子主題所有參數
	function getTitleNameMenu(formData, kindOfTheme, kindOfTitle){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertPractice.php",
			data:{
				"action" : "getTitleNameMenu",
				"kindOfTheme" : kindOfTheme
			},
			dataType: "json",
			success: function (json) {
				//console.log(json);
				if("init" == formData){
					$("#dataForm #kindOfTitle").empty();
					$("#dataForm #kindOfTitle").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.title.length; i++){
						$("#dataForm #kindOfTitle").append($("<option></option>").attr("value", json.title[i].title_code).text(json.title[i].title_name));
					}
				}
				if("save" == formData){
					$("#insertPracticeForm #kindOfTitle").empty();
					$("#insertPracticeForm #kindOfTitle").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.title.length; i++){
						$("#insertPracticeForm #kindOfTitle").append($("<option></option>").attr("value", json.title[i].title_code).text(json.title[i].title_name));
					}
				}
				if("update" == formData){
					$("#updatePracticeForm #kindOfTitle").empty();
					$("#updatePracticeForm #kindOfTitle").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.title.length; i++){
						$("#updatePracticeForm #kindOfTitle").append($("<option></option>").attr("value", json.title[i].title_code).text(json.title[i].title_name));
					}
					$("#updatePracticeForm #kindOfTitle").val(kindOfTitle);
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
			}
		});
	}