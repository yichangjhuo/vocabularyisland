var grid, onSuccessFunc = function( response ) {
	/*自訂項次*/
	var page = 1;
	var responseJson = JSON.parse(response);
	if( $("#divtab select[data-role='page-size']").val() < responseJson.total ) {
		page = $("#divtab input[data-role='page-number']").val() == 0 ? "1" : $("#divtab input[data-role='page-number']").val();
		console.dir( "page-number : " + page );
	}
	$.each(responseJson.records, function( index, value ) {
		var strPage = page -1 == 0 ? "" : (page-1).toString();
		responseJson.records[index]['rowId'] = index+1 == 10 ? parseInt( responseJson.records[index-1]['rowId'])+1 : strPage+(index+1);
	});
	//detail_grid render item number
	grid.render( responseJson );
	
	if( responseJson.rtnFlag == "ng" ){
		alert( responseJson.rtnMsg );
	}
};
$(document).ready(function () {
    let account = "";           // 輸入的帳號。
    let name = "";              // 使用者姓名。
    let number_of_card = 0;     // 字卡數量。
    var islands_status = [];    // 每個島嶼進度。
    
    
    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: true,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes !'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
            
        }else if(status == 1){
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";
        
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: false,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'O K'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
		}
    }
	
	/*抓使用者資訊。*/
    function getPersonalInformation() {
        console.log("開始抓使用者資訊...");
        
        $.ajax({
            type: "get",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                $('#name').text(name);
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                number_of_card = 0;
                $('#name').text(name);
            }
        });
        /* 未完成 */
    }
	
	/**
	 * 將表單資料序列化並轉成JSON型態
	 */
	$.fn.serializeFormJSON = function () {

		var o = {};
		var a = this.serializeArray();
		$.each(a, function () {
			if( o[this.name] ) {
				if( !o[this.name].push ) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

    /*上一頁*/
    $('#boat').on('click',function(){
        console.log('將回到主畫面。');
        location.replace("teacher.html");
    });
	
	grid = $('#grid').grid({
		dataSource: {
			type:"POST",
			url:'php/teacher_insertVocabulary.php',
			data:{
				action:'query'
			},
			success: onSuccessFunc
		},
		uiLibrary : 'bootstrap4',
		locale : 'zh-tw',
		columns: [{ 
			field: 'rowId', 
			title: '',
			width: '5%'
		}, {
			field: 'vl_vocabulary', 
			title: '單字(E)',
			width: '10%'
		}, {
			field: 'vl_definition', 
			title: '單字(C)'
		}, {
			field: 'vl_part_of_speech', 
			title: '詞性',
			width: '10%'
		}, {
			field: 'theme_name', 
			title: '主題',
			width: '15%'
		}, {
			field: 'title_name', 
			title: '子主題',
			width: '15%'
		}, {
			field: 'pt_name', 
			title: '自主練習',
			width: '15%'
		}, { 
			field: 'vl_id', hidden: true
		}, {
			field : "",
			title : "",
			tmpl : "<button type='button' id='updateBtn' class='btn btn-primary'><span class='btn-txt'>編輯</span></button>",
			events : {
				'click': update_view
			},
			width: '10%'
		}/*, {
			field : "",
			title : "",
			tmpl : "<button type='button' id='updateBtn' class='btn btn-primary'><span class='btn-txt'>刪除</span></button>",
			events : {
				'click': delete_view
			},
			width: '10%'
		}*/],
		pager: {
			limit: 3,
			sizes: [1, 3, 5]
		}
	});

	//新增dialog
	$( "#insertVocabulary_dialog" ).dialog({
		autoOpen: false,
		height: 500,
		width: 500,
		modal: false,
		buttons:{
			"新增":function(){
				insertVocabulary();
			},
			"取消":function(){
				$("#insertVocabulary_dialog" ).dialog("close");
			}
		}
    });
	//編輯dialog
	$( "#updateVocabulary_dialog" ).dialog({
		autoOpen: false,
		height: 500,
		width: 500,
		modal: false,
		buttons:{
			"更新":function(){
				updateVocabulary();
			},
			"取消":function(){
				$("#updateVocabulary_dialog" ).dialog("close");
			}
		}
    });
	//新增button
	$("#insertVocabularyBtn").on('click', function(){
		//open dialog
		getThemeNameMenu("save");
		$("#insertVocabularyForm")[0].reset();
		$("#insertVocabularyForm #insertShowView1").attr("src","");
		$("#insertVocabularyForm #insertShowView2").attr("src","");
		$("#insertVocabularyForm #insertShowView3").attr("src","");
		$("#insertVocabulary_dialog" ).dialog("open");
	});
	
	//查詢button
	$("#selectVocabularyBtn").on("click", function() {
		queryByPage();
	});
	
	//選擇查詢條件-主題
	$("#dataForm #vlTheme").on("change", function() {
		if($("#dataForm #vlTheme").val() != ''){
			getTitleNameMenu("init", $("#dataForm #vlTheme").val());
		}else{
			$("#dataForm #vlTitle").empty();
			$("#dataForm #vlTitle").append($("<option></option>").attr("value", "").text("請選擇"));
			$("#dataForm #vlPractice").empty();
			$("#dataForm #vlPractice").append($("<option></option>").attr("value", "").text("請選擇"));
		}
	});
	
	//新增dialog-主題
	$("#insertVocabularyForm #vlTheme").on("change", function() {
		if($("#insertVocabularyForm #vlTheme").val() != ''){
			getTitleNameMenu("save", $("#insertVocabularyForm #vlTheme").val());
		}else{
			$("#insertVocabularyForm #vlTitle").empty();
			$("#insertVocabularyForm #vlTitle").append($("<option></option>").attr("value", "").text("請選擇"));
			$("#insertVocabularyForm #vlPractice").empty();
			$("#insertVocabularyForm #vlPractice").append($("<option></option>").attr("value", "").text("請選擇"));
		}
	});
	
	//修改dialog-主題
	$("#updateVocabularyForm #vlTheme").on("change", function() {
		if($("#updateVocabularyForm #vlTheme").val() != ''){
			getTitleNameMenu("update", $("#updateVocabularyForm #vlTheme").val());
		}else{
			$("#updateVocabularyForm #vlTitle").empty();
			$("#updateVocabularyForm #vlTitle").append($("<option></option>").attr("value", "").text("請選擇"));
			$("#updateVocabularyForm #vlPractice").empty();
			$("#updateVocabularyForm #vlPractice").append($("<option></option>").attr("value", "").text("請選擇"));
		}
	});
	
	//選擇查詢條件-子主題
	$("#dataForm #vlTitle").on("change", function() {
		if($("#dataForm #vlTitle").val() != ''){
			getPracticeNameMenu("init", $("#dataForm #vlTheme").val(), $("dataForm #vlTitle").val());
		}else{
			$("#dataForm #vlPractice").empty();
			$("#dataForm #vlPractice").append($("<option></option>").attr("value", "").text("請選擇"));
		}
	});
	
	//新增dialog-子主題
	$("#insertVocabularyForm #vlTitle").on("change", function() {
		if($("#insertVocabularyForm #vlTitle").val() != ''){
			getPracticeNameMenu("save", $("#insertVocabularyForm #vlTheme").val(), $("#insertVocabularyForm #vlTitle").val());
		}else{
			$("#insertVocabularyForm #vlPractice").empty();
			$("#insertVocabularyForm #vlPractice").append($("<option></option>").attr("value", "").text("請選擇"));
		}
	});
	
	//修改dialog-子主題
	$("#updateVocabularyForm #vlTitle").on("change", function() {
		if($("#updateVocabularyForm #vlTitle").val() != ''){
			getPracticeNameMenu("update", $("#updateVocabularyForm #vlTheme").val(), $("#updateVocabularyForm #vlTitle").val());
		}else{
			$("#updateVocabularyForm #vlPractice").empty();
			$("#updateVocabularyForm #vlPractice").append($("<option></option>").attr("value", "").text("請選擇"));
		}
	});
	
	$(".fileImg").on("change", function() {
		if($("#"+ this.id).val() != ''){
			var fileName = $("#"+ this.id).val();
			var secondFileName = fileName.substr(fileName.lastIndexOf(".")+1);
			if(secondFileName != "jpg"){
				icon = "error";
				title = "上傳檔案附檔名錯誤";
				text = "請上傳jpg檔!!";
				swal.fire({
					icon: icon,
					title: title,
					text: text,
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'O K'
				}).then((result) => {
				});
			}else{
				readURL(this);
			}
		}else{
			$("#showView1").attr("src", "");
		}
	});
	
	function readURL(input){
		if(input.files && input.files[0]){
			var reader = new FileReader();
			reader.onload = function (e) {
				if("insertUpload_img1" == input.id){
					$("#insertShowView1").attr('src', e.target.result);
				}
				if("insertUpload_img2" == input.id){
					$("#insertShowView2").attr('src', e.target.result);
				}
				if("insertUpload_img3" == input.id){
					$("#insertShowView3").attr('src', e.target.result);
				}
				
				if("updateUpload_img1" == input.id){
					$("#updateShowView1").attr('src', e.target.result);
				}
				if("updateUpload_img2" == input.id){
					$("#updateShowView2").attr('src', e.target.result);
				}
				if("updateUpload_img3" == input.id){
					$("#updateShowView3").attr('src', e.target.result);
				}
				
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	
	
    /*---------------------------------------------------------------------*/
	
    getPersonalInformation();
	getThemeNameMenu("init");
});

	//查詢
	function queryByPage() {
		grid.reload( $("#dataForm").serializeFormJSON() );
	}
	
	//顯示編輯dialog
	function update_view(e){
		getThemeNameMenu("update", e.data.record.vl_theme);
		getTitleNameMenu("update", e.data.record.vl_theme, e.data.record.vl_title);
		getPracticeNameMenu("update", e.data.record.vl_theme, e.data.record.vl_title, e.data.record.vl_practice);
		
		$("#updateVocabularyForm #vlId").val(e.data.record.vl_id);
		$("#updateVocabularyForm #vlVocabulary").val(e.data.record.vl_vocabulary);
		$("#updateVocabularyForm #vlDefinition").val(e.data.record.vl_definition);
		$("#updateVocabularyForm #vlPartOfSpeech").val(e.data.record.vl_part_of_speech);

		$("#updateShowView1").attr('src', "word_image/" + e.data.record.vl_vocabulary + "_0.jpg");
		$("#updateShowView2").attr('src', "word_image/" + e.data.record.vl_vocabulary + "_1.jpg");
		$("#updateShowView3").attr('src', "word_image/" + e.data.record.vl_vocabulary + "_2.jpg");
		$("#updateVocabulary_dialog" ).dialog("open");
	}
	//刪除
	function delete_view(e){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertVocabulary.php",
			data:{
				action : "delete",
				vlId : e.data.record.vl_id,
				vlVocabulary : e.data.record.vl_vocabulary
			},
			dataType: "json",
			success: function (json) {
				if(json.status = 'success'){
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					}).then((result) => {
						if (result.value) {
							grid.reload( $("#dataForm").serializeFormJSON() );
						}
					});
				}else{
					swal.fire({
						icon: "error",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'O K'
					});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
				swal.fire({
					icon: "error",
					title: "刪除失敗",
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'O K'
				});
			}
		});
	}
	
	//select option init取主題所有參數
	function getThemeNameMenu(formData, vlTheme){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertVocabulary.php",
			data:{
				action:"getThemeNameMenu"
			},
			dataType: "json",
			success: function (json) {
				if("init" == formData){
					$("#dataForm #vlTheme").empty();
					$("#dataForm #vlTheme").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.theme.length; i++){
						$("#dataForm #vlTheme").append($("<option></option>").attr("value", json.theme[i].theme_code).text(json.theme[i].theme_name));
					}
				}
				if("save" == formData){
					$("#insertVocabularyForm #vlTheme").empty();
					$("#insertVocabularyForm #vlTheme").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.theme.length; i++){
						$("#insertVocabularyForm #vlTheme").append($("<option></option>").attr("value", json.theme[i].theme_code).text(json.theme[i].theme_name));
					}
				}
				if("update" == formData){
					$("#updateVocabularyForm #vlTheme").empty();
					$("#updateVocabularyForm #vlTheme").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.theme.length; i++){
						$("#updateVocabularyForm #vlTheme").append($("<option></option>").attr("value", json.theme[i].theme_code).text(json.theme[i].theme_name));
					}
					$("#updateVocabularyForm #vlTheme").val(vlTheme);
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
			}
		});
	}
	
	//select option init取子主題所有參數
	function getTitleNameMenu(formData, queryVlTheme, update_vlTitle){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertVocabulary.php",
			data:{
				action : "getTitleNameMenu",
				vlTheme: queryVlTheme
			},
			dataType: "json",
			success: function (json) {
				if("init" == formData){
					$("#dataForm #vlTitle").empty();
					$("#dataForm #vlTitle").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.title.length; i++){
						$("#dataForm #vlTitle").append($("<option></option>").attr("value", json.title[i].title_code).text(json.title[i].title_name));
					}
				}
				if("save" == formData){
					$("#insertVocabularyForm #vlTitle").empty();
					$("#insertVocabularyForm #vlTitle").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.title.length; i++){
						$("#insertVocabularyForm #vlTitle").append($("<option></option>").attr("value", json.title[i].title_code).text(json.title[i].title_name));
					}
				}
				if("update" == formData){
					$("#updateVocabularyForm #vlTitle").empty();
					$("#updateVocabularyForm #vlTitle").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.title.length; i++){
						$("#updateVocabularyForm #vlTitle").append($("<option></option>").attr("value", json.title[i].title_code).text(json.title[i].title_name));
					}
					$("#updateVocabularyForm #vlTitle").val(update_vlTitle);
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
			}
		});
	}
	
	//select option init取自主練習所有參數
	function getPracticeNameMenu(formData, queryVlTheme, queryVlTitle, update_vlPractice){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertVocabulary.php",
			data:{
				action : "getPracticeNameMenu",
				vlTheme : queryVlTheme,
				vlTitle : queryVlTitle
			},
			dataType: "json",
			success: function (json) {
				if("init" == formData){
					$("#dataForm #vlPractice").empty();
					$("#dataForm #vlPractice").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.title.length; i++){
						$("#dataForm #vlPractice").append($("<option></option>").attr("value", json.title[i].pt_code).text(json.title[i].pt_name));
					}
				}
				if("save" == formData){
					$("#insertVocabularyForm #vlPractice").empty();
					$("#insertVocabularyForm #vlPractice").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.title.length; i++){
						$("#insertVocabularyForm #vlPractice").append($("<option></option>").attr("value", json.title[i].pt_code).text(json.title[i].pt_name));
					}
				}
				if("update" == formData){
					$("#updateVocabularyForm #vlPractice").empty();
					$("#updateVocabularyForm #vlPractice").append($("<option></option>").attr("value", "").text("請選擇"));
					for(var i = 0; i < json.title.length; i++){
						$("#updateVocabularyForm #vlPractice").append($("<option></option>").attr("value", json.title[i].pt_code).text(json.title[i].pt_name));
					}
					$("#updateVocabularyForm #vlPractice").val(update_vlPractice);
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
			}
		});
	}
	
	function insertVocabulary(){
		var msg = "";
		if($("#insertVocabularyForm #vlTheme").val() == ''){
			msg += "請選擇主題，";
		}
		if($("#insertVocabularyForm #vlTitle").val() == ''){
			msg += "請選擇子主題，";
		}
		if($("#insertVocabularyForm #vlPractice").val() == ''){
			msg += "請選擇自主練習，";
		}
		if($("#insertVocabularyForm #vlVocabulary").val() == ''){
			msg += "請輸入單字，";
		}
		if($("#insertVocabularyForm #vlDefinition").val() == ''){
			msg += "請輸入單字中文名稱，";
		}
		if($("#insertVocabularyForm #vlPartOfSpeech").val() == ''){
			msg += "請輸入詞性，";
		}
		if($("#insertVocabularyForm #insertUpload_img1").val() == ''){
			msg += "請選擇第一張圖檔，";
		}
		if($("#insertVocabularyForm #insertUpload_img2").val() == ''){
			msg += "請選擇第二張圖檔，";
		}
		if($("#insertVocabularyForm #insertUpload_img3").val() == ''){
			msg += "請選擇第三張圖檔，";
		}
		if(msg != ''){
			swal.fire({
				icon: "error",
				title: "請輸入欄位",
				text:msg,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'OK !'
			});
			return;
		}
		var formData = new FormData($("#insertVocabularyForm")[0]);
		var file_data1 = $('#insertUpload_img1').prop('files')[0];
		formData.append('insertUpload_img1', file_data1);
		var file_data2 = $('#insertUpload_img2').prop('files')[0];
		formData.append('insertUpload_img2', file_data2);
		var file_data3 = $('#insertUpload_img3').prop('files')[0];
		formData.append('insertUpload_img3', file_data3);
		$.ajax({
			type: "post",
			url: "php/teacher_insertVocabulary.php",
			data: formData,
			async: false, //async設定true會變成異步請求。
			cache: false,
			processData: false,
			contentType: false,
			dataType:"json",
			success: function (json) {
				if(json.status = 'success'){
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					}).then((result) => {
						if (result.value) {
							$("#insertVocabulary_dialog" ).dialog("close");
							grid.reload( $("#dataForm").serializeFormJSON() );
						}
					});
					
				}else{
					swal.fire({
						icon: "error",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'O K'
					});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
				swal.fire({
					icon: "error",
					title: "新增單字失敗",
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'O K'
				});
			}
		});
	}
	
	function updateVocabulary(){
		var msg = "";
		if($("#updateVocabularyForm #vlTheme").val() == ''){
			msg += "請選擇主題，";
		}
		if($("#updateVocabularyForm #vlTitle").val() == ''){
			msg += "請選擇子主題，";
		}
		if($("#updateVocabularyForm #vlPractice").val() == ''){
			msg += "請選擇自主練習，";
		}
		if($("#updateVocabularyForm #vlVocabulary").val() == ''){
			msg += "請輸入單字，";
		}
		if($("#updateVocabularyForm #vlDefinition").val() == ''){
			msg += "請輸入單字中文名稱，";
		}
		if($("#updateVocabularyForm #vlPartOfSpeech").val() == ''){
			msg += "請輸入詞性，";
		}
		if(msg != ''){
			swal.fire({
				icon: "error",
				title: "請輸入欄位",
				text:msg,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'OK !'
			});
			return;
		}
		var formData = new FormData($("#updateVocabularyForm")[0]);
		var file_data1 = $('#updateUpload_img1').prop('files')[0];
		formData.append('updateUpload_img1', file_data1);
		var file_data2 = $('#updateUpload_img2').prop('files')[0];
		formData.append('updateUpload_img2', file_data2);
		var file_data3 = $('#updateUpload_img3').prop('files')[0];
		formData.append('updateUpload_img3', file_data3);
		$.ajax({
			type: "post",
			url: "php/teacher_insertVocabulary.php",
			data: formData,
			async: false, //async設定true會變成異步請求。
			cache: false,
			processData: false,
			contentType: false,
			dataType:"json",
			success: function (json) {
				if(json.status = 'success'){
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					}).then((result) => {
						if (result.value) {
							$("#updateVocabulary_dialog" ).dialog("close");
							grid.reload( $("#dataForm").serializeFormJSON() );
						}
					});
					
				}else{
					swal.fire({
						icon: "error",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'O K'
					});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
				swal.fire({
					icon: "error",
					title: "修改單字失敗",
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'O K'
				});
			}
		});
	}