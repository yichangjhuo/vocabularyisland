var grid, onSuccessFunc = function( response ) {
	/*自訂項次*/
	var page = 1;
	var responseJson = JSON.parse(response);
	if( $("#divtab select[data-role='page-size']").val() < responseJson.total ) {
		page = $("#divtab input[data-role='page-number']").val() == 0 ? "1" : $("#divtab input[data-role='page-number']").val();
		console.dir( "page-number : " + page );
	}
	$.each(responseJson.records, function( index, value ) {
		var strPage = page -1 == 0 ? "" : (page-1).toString();
		responseJson.records[index]['rowId'] = index+1 == 10 ? parseInt( responseJson.records[index-1]['rowId'])+1 : strPage+(index+1);
	});
	//detail_grid render item number
	grid.render( responseJson );
	
	if( responseJson.rtnFlag == "ng" ){
		alert( responseJson.rtnMsg );
	}
};
$(document).ready(function () {
    let account = "";           // 輸入的帳號。
    let name = "";              // 使用者姓名。
    let number_of_card = 0;     // 字卡數量。
    var islands_status = [];    // 每個島嶼進度。
    
    
    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: true,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes !'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
            
        }else if(status == 1){
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";
        
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: false,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'O K'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
		}
    }
	
	/*抓使用者資訊。*/
    function getPersonalInformation() {
        console.log("開始抓使用者資訊...");
        
        $.ajax({
            type: "get",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                $('#name').text(name);
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                number_of_card = 0;
                $('#name').text(name);
            }
        });
        /* 未完成 */
    }
	
	/**
	 * 將表單資料序列化並轉成JSON型態
	 */
	$.fn.serializeFormJSON = function () {

		var o = {};
		var a = this.serializeArray();
		$.each(a, function () {
			if( o[this.name] ) {
				if( !o[this.name].push ) {
					o[this.name] = [o[this.name]];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};

    /*上一頁*/
    $('#boat').on('click',function(){
        console.log('將回到主畫面。');
        location.replace("teacher.html");
    });
	
	grid = $('#grid').grid({
		dataSource: {
			type:"POST",
			url:'php/teacher_insertTheme.php',
			data:{
				action:'query'
			},
			success: onSuccessFunc
		},
		uiLibrary : 'bootstrap4',
		locale : 'zh-tw',
		columns: [{ 
			field: 'rowId', 
			title: '',
			width:50
		}, { 
			field: 'theme_name', title: '島嶼名稱'
		}, { 
			field: 'theme_id', hidden: true
		}, { 
			field: 'theme_code', hidden: true
		}, {
			field : "",
			title : "",
			tmpl: "<button type='button' id='updateBtn' class='btn btn-primary'><span class='btn-txt'>編輯</span></button>",
			events: {
				'click': update_view
			},
			width:100
		}/*, {
			field : "",
			title : "",
			tmpl: "<button type='button' id='deleteBtn' class='btn btn-primary'><span class='btn-txt'>刪除</span></button>",
			events: {
				'click': delete_fun
			},
			width:100
		}*/],
		pager: { 
			limit: 3,
			sizes: [1, 3, 5]
		}
	});


	$( "#insertTheme_dialog" ).dialog({
		autoOpen: false,
		height: 550,
		width: 550,
		modal: false,
		buttons:{
			"新增":function(){
				insertTheme();
				$("#insertTheme_dialog" ).dialog("close");
				grid.reload( $("#dataForm").serializeFormJSON() );
			},
			"取消":function(){
				$("#insertTheme_dialog" ).dialog("close");
			}
		}
    });
	
	$( "#updateTheme_dialog" ).dialog({
		autoOpen: false,
		height: 200,
		width: 350,
		modal: false,
		buttons:{
			"更新":function(){
				updateTheme();
				$("#updateTheme_dialog" ).dialog("close");
				grid.reload( $("#dataForm").serializeFormJSON() );
			},
			"取消":function(){
				$("#insertTheme_dialog" ).dialog("close");
			}
		}
    });
	//新增button
	$("#insertThemeBtn").on('click', function(){
		//open dialog
		$("#insertThemeForm")[0].reset();
		$("#insertTheme_dialog" ).dialog("open");
	});
	
	//查詢button
	$("#selectThemeBtn").on("click", function() {
		queryByPage();
	});
	
	//查詢
	function queryByPage() {
		
		grid.reload( $("#dataForm").serializeFormJSON() );
	}

	function insertTheme(){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertTheme.php",
			data:$("#insertThemeForm").serializeFormJSON(),
			dataType: "json",
			success: function (json) {
				//console.log(json);
				if(json.status = 'success'){
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					}).then((result) => {
						if (result.value) {
							$("#insertTitle_dialog" ).dialog("close");
							grid.reload( $("#dataForm").serializeFormJSON() );
						}
					});
				}else{
					swal.fire({
						icon: "error",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'O K'
					});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
				swal.fire({
					icon: "error",
					title: "新增主題失敗",
					confirmButtonColor: '#3085d6',
					confirmButtonText: 'O K'
				});
			}
		});
	}
	function update_view(e){
		$("#updateThemeForm #themeId").val(e.data.record.theme_id);
		$("#updateThemeForm #themeName").val(e.data.record.theme_name);
		$("#updateThemeForm #themeCode").val(e.data.record.theme_code);
		$("#updateTheme_dialog" ).dialog("open");
	}

	function updateTheme(){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertTheme.php",
			data:$("#updateThemeForm").serializeFormJSON(),
			dataType: "json",
			success: function (json) {
				if(json.status = 'success'){
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					}).then((result) => {
						if (result.value) {
							$("#insertTitle_dialog" ).dialog("close");
							grid.reload( $("#dataForm").serializeFormJSON() );
						}
					});
				}else{
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
			}
		});
	}
	
	function delete_fun(e){
		icon = "warning";
		text = "你確定要刪除嗎？";
		swal.fire({
			icon: icon,
			title: "",
			text: text,
			allowOutsideClick: false,
			allowEscapeKey: false,
			showCloseButton: true,
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes !'
		}).then((result) => {
			if(result.value){
				deleteTheme(e.data.record.theme_id, e.data.record.theme_code);
			}
		});
	}
	
	function deleteTheme(themeId, themeCode){
		$.ajax({
			type: "post",
			async: true, //async設定true會變成異步請求。
			cache: true,
			url: "php/teacher_insertTheme.php",
			data:{
				"action" : "delete",
				"themeId" : themeId,
				"themeCode" : themeCode
			},
			dataType: "json",
			success: function (json) {
				if(json.status = 'success'){
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					}).then((result) => {
						if (result.value) {
							grid.reload( $("#dataForm").serializeFormJSON() );
						}
					});
				}else{
					swal.fire({
						icon: "success",
						title: json.msg,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'OK !'
					});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
				console.log("textStatus:" + textStatus);
				console.log("errorThrown:" + errorThrown);
				console.log('Error.');
				swal.fire({
					icon: "success",
					title: json.msg,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'OK !'
				});
			}
		});
	}
	
    /*---------------------------------------------------------------------*/
	
    getPersonalInformation();    
});