$(function(){
    let account = "";           // 輸入的帳號。
    let name = "";              // 使用者姓名。
    let number_of_card = 0;     // 字卡數量。
    var islands_status = [];    // 每個島嶼進度。
    
    
    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
                    icon: icon,
                    title: title,
                    text: text,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes !'
                })
                .then((result) => {
                    if (result.value) {
                        window.location.assign("index.html");
                    }
                });
            
        }else if(status == 1){
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";
        
            swal.fire({
                    icon: icon,
                    title: title,
                    text: text,
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    showCloseButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'O K'
                })
                .then((result) => {
                    if (result.value) {
                        window.location.assign("index.html");
                    }
                });
        }        
    }
    
    /*抓使用者資訊。*/
    function getPersonalInformation() {
        console.log("開始抓使用者資訊...");
        
        $.ajax({
            type: "get",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                $('#name').text(name);
                $('#flashcard').text(number_of_card);
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                number_of_card = 0;
                $('#name').text(name);
                $('#flashcard').text(number_of_card);
            }
        });
        /* 未完成 */
    }    
    
    /*進入島嶼*/
    $('.island').on('click',function(){
        //let ID = $(this).attr('id');
        let ID = $(this).find("#island").val();
		if(ID != ''){
			console.log('將要進入 '+ID+' 島');
			window.location.assign("island.html?ID=" + ID);
			//window.location.assign(ID+"_island.html");
		}
    });
    
    /*進入背包*/
    $('#bag').on('click',function(){
        console.log('將要進入 背包 ');
        window.location.assign("bag.html");
    });

    /*上一頁*/
    $('#boat').on('click',function(){
        console.log('將回到登入畫面。');
        dialog(0);
    });
    
    
    
    /*---------------------------------------------------------------------*/
    
    
    
    
    getPersonalInformation();
    //getStatus(0,0,2);
    //getStatus(0,1,1);
	queryIsLands();
});

function queryIsLands(){
	console.log("開始抓島嶼...");

	$.ajax({
		type: "post",
		async: true, //async設定true會變成異步請求。
		cache: true,
		url: "php/get_island_information.php",
		data:{
			"action":"queryTheme"
		},
		dataType: "json",
		success: function (json) {
			console.log('抓每個島嶼 Success.');
			for(var i = 0; i < json['theme'].length; i++){
				//console.log(json['theme'][i]);
				$("#island" + i + " #islandName").text(json['theme'][i].theme_name);
				$("#island" + i + " #island").val(json['theme'][i].theme_name);
				$("#island" + i + " .island-image").attr('src','material/animal-island/island.png');
				$("#island" + i + " #content-image").attr('src','material/animal-island/road_and_building.png');
				$("#island" + i + " .buliding").attr('src','material/animal-island/building-0.png');
				queryLandsOfTitle(json['theme'][i].theme_code);
			}
                
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
			console.log("textStatus:" + textStatus);
			console.log("errorThrown:" + errorThrown);
		}
	});
}

function queryLandsOfTitle(themeCode){
	console.log("開始抓島嶼...");

	$.ajax({
		type: "post",
		async: true, //async設定true會變成異步請求。
		cache: true,
		url: "php/get_island_information.php",
		data:{
			"action" : "queryTitle",
			"kindOfTheme" : themeCode
		},
		dataType: "json",
		success: function (json) {
			//console.log('抓每個島嶼 Success.');
			//console.log(json['title']);
			for(var i = 0; i < json['title'].length; i++){
				//building-3-0 ~ building-3-3圖檔亂數
				var maxNum = 3;
				var minNum = 0;
				var n = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum;
				getStatus(json['title'][i].kind_of_theme,json['title'][i].title_code, n);
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
			console.log("textStatus:" + textStatus);
			console.log("errorThrown:" + errorThrown);
		}
	});
}

	/*抓每個島嶼的關卡進度。*/
	function getStatus(theme_code,title_code,building_code){
		console.log("開始抓島嶼的關卡進度...");
		/* building_code = 0~3 */
		$.ajax({
            type: "post",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/get_island_information.php",
            data:{
				action:"queryPractice",
                theme:theme_code,
                title:title_code
            },
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('抓每個島嶼的關卡進度 Success.');
                //console.log('抓每個島嶼的關卡進度:'+json['percentage']);
                if(json['percentage'] == 100){
                    $('#buliding-'+theme_code+'-'+title_code).attr('src','material/animal-island/building-3-'+building_code+'.png');
                }else if(json['percentage'] > 50 && json['percentage'] < 100){
                    $('#buliding-'+theme_code+'-'+title_code).attr('src','material/animal-island/building-2.png');
                }else if(json['percentage'] > 0 && json['percentage'] <= 50){
                    $('#buliding-'+theme_code+'-'+title_code).attr('src','material/animal-island/building-1.png');
                }else{
                    $('#buliding-'+theme_code+'-'+title_code).attr('src','material/animal-island/building-0.png');
                }
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
            }
        });
        /* 未完成 */
    }