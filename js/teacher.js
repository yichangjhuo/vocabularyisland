$(function(){
    let account = "";           // 輸入的帳號。
    let name = "";              // 使用者姓名。
    let number_of_card = 0;     // 字卡數量。
    var islands_status = [];    // 每個島嶼進度。
    
    
    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: true,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes !'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
            
        }else if(status == 1){
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";
        
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: false,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'O K'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
		}
    }
	
	/*抓使用者資訊。*/
    function getPersonalInformation() {
        console.log("開始抓使用者資訊...");
        
        $.ajax({
            type: "get",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                $('#name').text(name);
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                number_of_card = 0;
                $('#name').text(name);
            }
        });
        /* 未完成 */
    }
	
	/*進入新增主題*/
    $('#insertTheme').on('click',function(){
		console.log('將要進入 新增主題');
		window.location.assign("teacher_insertTheme.html");
    });
	/*進入新增子主題*/
    $('#insertSecondTheme').on('click',function(){
		console.log('將要進入 新增子主題');
		window.location.assign("teacher_insertTitle.html");
    });
	/*進入新增自主練習*/
    $('#insertPractice').on('click',function(){
		console.log('將要進入 自主練習');
		window.location.assign("teacher_insertPractice.html");
    });
	/*進入新增單字*/
    $('#insertVocabulary').on('click',function(){
		console.log('將要進入 新增單字');
		window.location.assign("teacher_insertVocabulary.html");
    });
	/*進入學生學習歷程*/
    $('#learningCourse').on('click',function(){
		console.log('將要進入學生學習歷程');
		window.location.assign("teacher_watch_learningCourse.html");
    });

    /*上一頁*/
    $('#boat').on('click',function(){
        console.log('將回到登入畫面。');
        dialog(0);
    });
    /*---------------------------------------------------------------------*/
	
    getPersonalInformation();    
});