$(document).ready(function () {
    let account = "";           // 輸入的帳號。
    let name = "";              // 使用者姓名。
    let number_of_card = 0;     // 字卡數量。
    var islands_status = [];    // 每個島嶼進度。
    
    
    /* 跳出通知。*/
    function dialog(status) {
        let icon = "";
        let title = "";
        let text = "";
        let console_log = "";
        if (status == 0) {
            icon = "warning";
            title = "將前往";
            text = "是否前往【登入畫面】？";
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: true,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes !'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
            
        }else if(status == 1){
            icon = "warning";
            title = "哎呀！發現你沒登入哦！";
            text = "將前往【登入畫面】";
        
            swal.fire({
				icon: icon,
				title: title,
				text: text,
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCloseButton: false,
				confirmButtonColor: '#3085d6',
				confirmButtonText: 'O K'
			}).then((result) => {
				if (result.value) {
					window.location.assign("index.html");
				}
			});
		}
    }
	
	/*抓使用者資訊。*/
    function getPersonalInformation() {
        console.log("開始抓使用者資訊...");
        
        $.ajax({
            type: "get",
            async: true, //async設定true會變成異步請求。
            cache: true,
            url: "php/world.php",
            dataType: "json",
            success: function (json) {
                //jQuery會自動將結果傳入(如果有設定callback函式的話，兩者都會執行)
                console.log('Success.');
                name = json['name'];
                number_of_card = json['cardAmount'];
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                $('#name').text(name);
                
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
                console.log("textStatus:" + textStatus);
                console.log("errorThrown:" + errorThrown);
                console.log('Error.');
                
                console.log('Name:'+name);
                
                if(name == "" || name == null){ // 帳號未登入。
                    dialog(1);
                }
                
                number_of_card = 0;
                $('#name').text(name);
            }
        });
        /* 未完成 */
    }

    /*上一頁*/
    $('#boat').on('click',function(){
        console.log('將回到主畫面。');
        location.replace("teacher_watch_learningCourse.html");
    });
	
	$("#chartBtn").on('click',function(){
		getUrl();
       $("#divChart").css("display","block");
	   $("#divChart1").css("display","none");
    });
	
	$("#chart1Btn").on('click',function(){
		getUrl();
       $("#divChart1").css("display","block");
	   $("#divChart").css("display","none");
    });
	
	
    /*---------------------------------------------------------------------*/
	
    getPersonalInformation();
	
});
	function getUrl(){
		var url = location.href;
	
		//再來用去尋找網址列中是否有資料傳遞(QueryString)
		if(url.indexOf('?')!=-1){
			//之後去分割字串把分割後的字串放進陣列中
			var ary1 = url.split('?');
			//此時ary1裡的內容為：
			//console.log(ary1);
			//下一步把後方傳遞的每組資料各自分割
			var ary2 = ary1[1].split('=');
			//console.log(ary2);
			//取得id值
			var id = ary2[1].replace(/%20/g, " ");
			
			getPracticeStatus(id);
		}
	}

function getPracticeStatus(id){
	$.ajax({
		type: "post",
		async: true, //async設定true會變成異步請求。
		cache: true,
		url: "php/studentForLearningCourse.php",
		data: {
			"action":"queryMember",
			"psAccount":id
		},
		dataType: "json",
		success: function (json) {
			//console.log(json);
			//練習次數
            contentChartP(json.theme, json.student);
			//學過幾個單字
			contentChartV(json.themeForVocabulary, json.studentForVocabulary);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			console.log("XMLHttpRequest:" + XMLHttpRequest.responseText);
			console.log("textStatus:" + textStatus);
			console.log("errorThrown:" + errorThrown);
			console.log('Error.');
		}
	});
}

function contentChartP(jsonTheme, jsonStudent){
	var themeArray = new Array();
	for(var i =0; i < jsonTheme.length; i++){
		themeArray[i] = jsonTheme[i].theme_name;
	}
	var studentArray = new Array(jsonTheme.length);
	for(var i =0; i < jsonStudent.length; i++){
		studentArray[jsonStudent[i].ps_theme] = parseInt(jsonStudent[i].COUNT);
	}
	for(var i=0; i < studentArray.length; i++){
		if(studentArray[i] == undefined){studentArray[i] == 0;}
	}
	
	var ctx = $("#chart");
	var chart = new Chart(ctx, {
		type: 'bar',
		data: {
			//labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
			labels: themeArray,
			datasets: [{
				label: '練習次數',
				data: studentArray,
				backgroundColor: 'rgba(255, 206, 86, 0.2)',
				borderColor: 'rgba(255, 206, 86, 1)',
				borderWidth: 1
			}]
		}
	});
}

function contentChartV(themeForVocabulary, studentForVocabulary){
	//console.log(themeForVocabulary);
	//console.log(studentForVocabulary);
	
	var themeNameArray = new Array();
	var themeCountArray = new Array();
	for(var i = 0; i < themeForVocabulary.length;i++){
		themeNameArray[i] = themeForVocabulary[i].theme_name;
		themeCountArray[i] = parseInt(themeForVocabulary[i].COUNT);
	}
	var studentArray = new Array(studentForVocabulary.length);
	for(var i =0; i < studentForVocabulary.length; i++){
		studentArray[studentForVocabulary[i].au_theme] = parseInt(studentForVocabulary[i].count);
	}
	var backgroundColorArray = new Array();
	var borderColorArray = new Array();
	for(var i=0; i < studentArray.length; i++){
		if(studentArray[i] == undefined){studentArray[i] == 0;}
		backgroundColorArray[i] = 'rgba(54, 162, 235, 0.2)';
		backgroundColorArray[i] = 'rgba(54, 162, 235, 0.2)';
	}
	var ctx = $("#chart1");
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
		  //labels: ["王大毛", "孫小毛", "小甜甜", "許唇美", "豬哥亮"],
		  labels: themeNameArray,
		  datasets: [{
			label: '各主題單字量',
			//data: [4, 1, 3, 7, 2],
			data : themeCountArray,
			backgroundColor: 'rgba(255, 99, 132, 0.2)',
			borderColor: 'rgba(255,99,132,1)',
			borderWidth: 1
		  },{
			label: '學習過的單字量',
			data:studentArray,
			backgroundColor: 'rgba(54, 162, 235, 0.2)',
			borderColor: 'rgba(54, 162, 235, 1)',
			borderWidth: 1
		  }]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true,
					}
				}]
			}
		}
	});
}
